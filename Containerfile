# syntax=docker/dockerfile:1.3
FROM registry.fedoraproject.org/fedora-minimal:39

ARG BUILD_DATE
ARG VCS_REF

LABEL description="Container image based on fedora-minimal, hourly updated, serving as a base image for other projects"

# as per https://github.com/opencontainers/image-spec/blob/main/annotations.md,
# keep Label Schema labels for backward compatibility.
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer-containers/fedora-minimal.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=MIT \
      org.opencontainers.image.description="fedora-minimal" \
      org.opencontainers.image.description="Container image based on fedora-minimal, hourly updated, serving as a base image for other projects" \
      org.opencontainers.image.created=$BUILD_DATE \
      org.opencontainers.image.authors=wanderer \
      org.opencontainers.image.url="https://git.dotya.ml/wanderer-containers/fedora-minimal.git" \
      org.opencontainers.image.source="https://git.dotya.ml/wanderer-containers/fedora-minimal.git" \
      org.opencontainers.image.revision=$VCS_REF \
      org.opencontainers.image.licenses=MIT

# DL3041 warning: Specify version with `dnf install -y <package>-<version>`.
# hadolint ignore=DL3041
RUN microdnf --refresh upgrade -y && \
    microdnf install -y --nodocs --setopt install_weak_deps=0 \
	  findutils \
    && microdnf clean all -y; \
	\
    rm -rf /usr/share/zoneinfo/*; rm -rf /usr/include/*; \
    find /. -name "*~" -type f -delete > /dev/null 2>&1; \
    find /usr/share/terminfo/. -type f -delete \
	! -name "*xterm*" ! -name "*screen*" ! -name "*screen*"
WORKDIR /

# vim: ft=dockerfile
