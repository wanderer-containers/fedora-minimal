# fedora-minimal
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/fedora-minimal/status.svg)](https://drone.dotya.ml/wanderer-containers/fedora-minimal)

this repo provides a Containerfile to build a container image based on
`registry.fedoraproject.org/fedora-minimal`, nightly updated, serving as a
base image for other projects

currently based on fedora version: `39`

### LICENSE
MIT
